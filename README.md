# Volirium-Breakpad

This is a fork of [Google Breakpad](https://chromium.googlesource.com/breakpad/breakpad), with some additions:

* The linux-syscall-support header from [Google linux-syscall-support](https://chromium.googlesource.com/linux-syscall-support) is directly included

